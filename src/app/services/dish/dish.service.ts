import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IDish } from '../../models/dish/IDish';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor() { }

  getDishes(): Observable<IDish[]> {
    const dishes: IDish[] = [
      {
        title: 'Dish 1',
        description: 'Lorem ipsum ...'
      },
      {
        title: 'Dish 2',
        description: 'Ipsum etr t ...'
      },
    ]
    return of(dishes);
  }
}
