import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IUser } from '../../models/user/IUser';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUsers(): Observable<IUser[]> {
    const users: IUser[] = [
      {
        firstName: 'User 1',
        age: 18
      },
      {
        firstName: 'User 2',
        age: 21
      }
    ]
    return of(users);
  }
}
