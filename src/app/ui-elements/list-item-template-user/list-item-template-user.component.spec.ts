import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemTemplateUserComponent } from './list-item-template-user.component';

describe('ListItemTemplateUserComponent', () => {
  let component: ListItemTemplateUserComponent;
  let fixture: ComponentFixture<ListItemTemplateUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemTemplateUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemTemplateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
