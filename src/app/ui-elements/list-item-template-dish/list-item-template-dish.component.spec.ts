import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemTemplateDishComponent } from './list-item-template-dish.component';

describe('ListItemTemplateDishComponent', () => {
  let component: ListItemTemplateDishComponent;
  let fixture: ComponentFixture<ListItemTemplateDishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemTemplateDishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemTemplateDishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
