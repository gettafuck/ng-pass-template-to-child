import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input('dataItems') _dataItems: any[];

  @Input('templateRef') _templateRef: any;

  constructor(
    private __templateRef: TemplateRef<any>
  ) { }

  ngOnInit() {
    // this.__templateRef = this._templateRef;
  }

}
