export interface IDish {
	title: string;
	description: string;
}