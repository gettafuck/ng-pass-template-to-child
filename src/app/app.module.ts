import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListModule } from './ui-elements/list/list.module';
import { ListItemTemplateUserComponent } from './ui-elements/list-item-template-user/list-item-template-user.component';
import { ListItemTemplateDishComponent } from './ui-elements/list-item-template-dish/list-item-template-dish.component';

@NgModule({
  declarations: [
    AppComponent,
    ListItemTemplateUserComponent,
    ListItemTemplateDishComponent
  ],
  imports: [
    BrowserModule,
    ListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
