import { Component, OnInit } from '@angular/core';

import { UserService } from './services/user/user.service';
import { DishService } from './services/dish/dish.service';

import { IUser } from './models/user/IUser';
import { IDish } from './models/dish/IDish';

import { ListItemTemplateUserComponent } from './ui-elements/list-item-template-user/list-item-template-user.component';
import { ListItemTemplateDishComponent } from './ui-elements/list-item-template-dish/list-item-template-dish.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  users: IUser[];
  dishes: IDish[];

  UserItemTemplateRef: any;
  DishItemTemplateRef: any;

  constructor(
    private userService: UserService,
    private dishService: DishService
  ) {}

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.users = users);

    this.dishService.getDishes()
      .subscribe(dishes => this.dishes = dishes);

    this.UserItemTemplateRef = ListItemTemplateUserComponent;
    this.DishItemTemplateRef = ListItemTemplateDishComponent;
  }

}
